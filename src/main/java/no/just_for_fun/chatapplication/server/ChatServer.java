package no.just_for_fun.chatapplication.server;

import no.just_for_fun.chatapplication.server.core.UserThread;
import no.just_for_fun.chatapplication.server.exceptions.MessageException;
import no.just_for_fun.chatapplication.server.exceptions.ServerException;
import no.just_for_fun.chatapplication.server.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

public class ChatServer {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private int port;
    private Set<String> userNames = new HashSet<>();
    private Set<UserThread> userThreads = new HashSet<>();

    public ChatServer(int port) {
        this.port = port;
    }

    public static void main(String[] args) throws ServerException {
        new ChatServer(8080).run();
    }

    public void run() throws ServerException {
        try (ServerSocket serverSocket = new ServerSocket(port)) {

            LOGGER.info(" * Listening on port " + port);

            while (serverSocket.isBound() && !serverSocket.isClosed()) {
                Socket socket = serverSocket.accept();
                LOGGER.info(" * Connection accepted: " + socket.getInetAddress());

                LOGGER.debug("Initiating user thread");
                UserThread userThread = new UserThread(socket,this);

                LOGGER.debug("Adding thread to list");
                userThreads.add(userThread);

                LOGGER.debug("Starting user thread");
                userThread.start();

            }

        } catch (IOException e) {
            // TODO make proper error messages, bruh
            throw new ServerException("Problem with server, you know where bruh, " + e.getMessage());
        }
    }

    public void broadcast(Message message) throws MessageException {
        LOGGER.info("[BROADCAST] " + message.getMessage());
        for (UserThread user : userThreads) {
            user.sendMessage(message);
        }
    }

    public void broadcast(Message message, UserThread excludeUser) throws MessageException {
        LOGGER.info("[BROADCAST] " + message.getMessage());
        for (UserThread user : userThreads) {
            if (user != excludeUser) {
                user.sendMessage(message);
            }
        }
    }

    public void addUserName(String name) {
        userNames.add(name);
    }

    public void removeUser(String userName, UserThread user) {
        userNames.remove(userName);
        userThreads.remove(user);
    }

    public Set<String> getUserNames() {
        return userNames;
    }

    public boolean hasUsers() {
        return !userNames.isEmpty();
    }

}
