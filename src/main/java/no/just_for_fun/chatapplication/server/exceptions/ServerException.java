package no.just_for_fun.chatapplication.server.exceptions;

public class ServerException extends Exception {
    public ServerException(String message) {
        super(message);
    }
}
