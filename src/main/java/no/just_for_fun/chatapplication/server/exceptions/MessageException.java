package no.just_for_fun.chatapplication.server.exceptions;

public class MessageException extends Exception {
    public MessageException(String message) {
        super(message);
    }
}
