package no.just_for_fun.chatapplication.server.exceptions;

public class InvalidUserNameException extends Exception {
    public InvalidUserNameException(String message) {
        super(message);
    }
}
