package no.just_for_fun.chatapplication.server.message;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Message implements Serializable {

    private final String message;
    private final LocalDateTime timeOfMessage;
    private final boolean isRequest;
    private final Request request;

    /**
     * Defines a message to be sent between client and server.
     *
     * @param message the message to be sent.
     */
    public Message(String message) {
        this.isRequest = false;
        this.message = message;
        this.timeOfMessage = LocalDateTime.now();
        this.request = Request.NONE;
    }

    /**
     * Defines a message to be sent between client and server.
     * If message is a request, a request is made from a RequestFactory using to message-string.
     * Request can be the following:
     * REQUEST_CLIENT_USERNAME, REQUEST_CONNECTED_CLIENTS, REQUEST_DISCONNECT, NONE
     *
     * @param message the message to be sent, can also be a request.
     * @param isRequest defines if the message is a request or a normal message.
     */
    public Message(String message, boolean isRequest) {
        this.isRequest = isRequest;
        this.message = message;
        this.timeOfMessage = LocalDateTime.now();
        this.request = RequestFactory.createRequest(message);
    }

    // Getters:
    public String getMessage() {
        return message;
    }

    public LocalDateTime getTimeOfMessage() {
        return timeOfMessage;
    }

    public boolean isRequest() {
        return isRequest;
    }

    public Request getRequest() {
        return request;
    }
}
