package no.just_for_fun.chatapplication.server.message;

public enum Request {
    REQUEST_CLIENT_USERNAME, REQUEST_CONNECTED_CLIENTS, REQUEST_DISCONNECT, NONE
}
