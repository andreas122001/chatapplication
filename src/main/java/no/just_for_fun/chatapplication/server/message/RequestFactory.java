package no.just_for_fun.chatapplication.server.message;

public class RequestFactory {
    public static Request createRequest(String request) {
        switch (request) {
            case "REQUEST_CLIENT_USERNAME":
                return Request.REQUEST_CLIENT_USERNAME;
            case "REQUEST_CONNECTED_CLIENTS":
                return Request.REQUEST_CONNECTED_CLIENTS;
            case "REQUEST_DISCONNECT":
                return Request.REQUEST_DISCONNECT;
            default:
                return Request.NONE;
        }
    }
}
