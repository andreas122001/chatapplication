package no.just_for_fun.chatapplication.server.core;

import no.just_for_fun.chatapplication.server.ChatServer;
import no.just_for_fun.chatapplication.server.exceptions.MessageException;
import no.just_for_fun.chatapplication.server.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class UserThread extends Thread {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private final Socket socket;
    private final ChatServer server;
    private ObjectOutputStream out;
    private String userName;

    public UserThread(Socket socket, ChatServer server) {
        this.socket = socket;
        this.server = server;
    }

    @Override
    public void run() {
        try {
            //Sets up reader/writer
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            out = new ObjectOutputStream(socket.getOutputStream());

            // loops until username is read
            boolean userNameIsInvalid = true;
            while (userNameIsInvalid) {
                requestUserName();
                Message userNameInput = (Message) in.readObject();
                String userName = userNameInput.getMessage();
                if (userName != null && !userName.isBlank()) {
                    userNameIsInvalid = false;
                    this.userName = userName;
                    // Adds username to server
                    server.addUserName(userName);
                }
            }

            // Broadcast to other users
            Message serverMessage = new Message(userName + " connected!");
            server.broadcast(serverMessage);

            Message userMessage;
            do {
                userMessage = (Message) in.readObject();
                if (userMessage.isRequest()) {
                    handleRequest(userMessage);
                } else {
                    sendToServer(userMessage);
                }
            } while (!socket.isClosed() && socket.isBound());

        } catch (IOException | MessageException e) {
            LOGGER.info("User lost connection: " + socket.getInetAddress());
            try {
                disconnectUser();
            } catch (IOException | MessageException ioException) {
                ioException.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            LOGGER.error("Input is not of class Message");
        }
    }

    public void requestUserName() throws MessageException {
        sendRequest("REQUEST_CLIENT_USERNAME");
    }

    public void sendToServer(Message message) throws MessageException {
        Message serverMessage = new Message("["+userName+"]: " + message.getMessage());
        server.broadcast(serverMessage);
    }

    public void handleRequest(Message request) throws MessageException {
        switch (request.getRequest()) {
            case REQUEST_CONNECTED_CLIENTS:
                sendMessage(getUserNamesFromServer());
                break;
            case REQUEST_DISCONNECT:
                try {
                    disconnectUser();
                } catch (IOException | MessageException e) {
                    System.err.println("Error on disconnect (UserThread)");
                }
                break;
            default:
                // TODO handle
                System.err.println("Invalid request: " + request.getRequest());
        }
    }

    public void disconnectUser() throws IOException, MessageException {
        server.removeUser(userName, this);
        Message serverMessage = new Message(userName + " disconnected.");
        server.broadcast(serverMessage,this);
        socket.close();
    }

    public void sendRequest(String request) throws MessageException {
        try {
            Message requestMessage = new Message(request,true);
            out.writeObject(requestMessage);
        } catch (IOException e) {
            throw new MessageException("Failed to send message in thread " + this);
        }
    }

    public void sendMessage(Message message) throws MessageException {
        try {
            out.writeObject(message);
        } catch (IOException e) {
            throw new MessageException("Failed to send message in thread " + this);
        }
    }

    public Message getUserNamesFromServer() {
        StringBuilder stringBuilder = new StringBuilder();
        for (String name : server.getUserNames()) {
            stringBuilder.append(name).append("\n");
        }
        return new Message(stringBuilder.toString());
    }
}
