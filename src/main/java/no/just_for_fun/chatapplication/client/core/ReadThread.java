package no.just_for_fun.chatapplication.client.core;

import no.just_for_fun.chatapplication.client.ChatClient;
import no.just_for_fun.chatapplication.server.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;

public class ReadThread extends Thread {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private ObjectInputStream reader;
    private final Socket socket;
    private final ChatClient client;

    public ReadThread(Socket socket, ChatClient client) {
        this.socket = socket;
        this.client = client;
    }

    public void run() {
        // Sets up InputStream
        try {
            InputStream input = socket.getInputStream();
            reader = new ObjectInputStream(input);
        } catch (IOException ex) {
            System.out.println("Error getting input stream: " + ex.getMessage());
            ex.printStackTrace();
        }

        // Listens for input
        while (socket.isBound() && !socket.isClosed()) {
            try {
                // Reads response from sever
                Message response = (Message) reader.readObject();
                // Handles response
                client.handleMessage(response);
            } catch (IOException e) {
                LOGGER.error("Server cannot be reached",e);
                client.handleLostConnection();
                break;
            } catch (ClassNotFoundException e) {
                LOGGER.error("Message is of wrong class",e);
            }
        }
    }
}
