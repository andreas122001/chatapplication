package no.just_for_fun.chatapplication.client.excpetions;

public class ConnectionException extends Exception {
    public ConnectionException(String message) {
        super(message);
    }
}
