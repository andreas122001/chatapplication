package no.just_for_fun.chatapplication.client;

import no.just_for_fun.chatapplication.client.core.ReadThread;
import no.just_for_fun.chatapplication.server.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class ChatClient extends Thread {

    // TUTORIAL
    // https://www.codejava.net/java-se/networking/how-to-create-a-chat-console-application-in-java-using-socket

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private final String hostname;
    private final int port;
    private String userName = "User";
    private ObjectOutputStream out;
    private App app;
    private Socket socket;

    public ChatClient(String hostname, int port) {
        this.hostname = hostname;
        this.port = port;
    }

    public ChatClient(String hostname, int port, App app) {
        this.hostname = hostname;
        this.port = port;
        this.app = app;
    }

    public static void main(String[] args) {
        new ChatClient("localhost",8080).start();
    }

    @Override
    public void run() {
        try {
            socket = new Socket(hostname, port);

            out = new ObjectOutputStream(socket.getOutputStream());

            LOGGER.info(" * Server connected!");

            ReadThread readThread = new ReadThread(socket, this);
            readThread.start();

        } catch (UnknownHostException ex) {
            LOGGER.error("Server not found",ex);
            printMessage(new Message("Server not found. Try restarting."));
        } catch (IOException e) {
            LOGGER.error("I/O error",e);
            printMessage(new Message("Could not connect to server. Try restarting."));
        }
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return this.userName;
    }

    public void sendMessage(Message message) throws IOException {
        LOGGER.info("[SEND] " + message.getMessage());
        out.writeObject(message);
    }

    public void sendRequest(Message message) throws IOException {
        LOGGER.info("[SEND REQUEST] " + message.getMessage());
        out.writeObject(message);
    }

    public void printMessage(Message message) {
        LOGGER.info("[Print] " + message.getMessage());
        app.printMessage(message);
    }

    public void handleMessage(Message message) throws IOException {
        if (message.isRequest()) {
            handleRequest(message);
        } else {
            printMessage(message);
        }
    }

    public void handleRequest(Message request) throws IOException {
        LOGGER.info("[Request] " + request.getMessage());
        switch (request.getRequest()) {
            case REQUEST_CLIENT_USERNAME:
                sendMessage(new Message(app.getUserName()));
                break;
            case REQUEST_DISCONNECT:
                disconnectUser();
                break;
            case NONE:
                System.out.println("nvm previous request, dude");
                break;
        }
    }

    public void handleLostConnection() {
        printMessage(new Message(" >> Lost connection to server! Try restarting application."));
    }

    public void disconnectUser() throws IOException {
        if (socket != null) {
            LOGGER.info("Disconnecting...");
            Message disconnectRequest = new Message("REQUEST_DISCONNECT", true);
            out.writeObject(disconnectRequest);
        } else {
            LOGGER.info("Socket not connected, nothing to disconnect");
        }
    }
}
