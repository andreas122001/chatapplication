package no.just_for_fun.chatapplication.client.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import no.just_for_fun.chatapplication.client.TestApp;

import java.io.IOException;

public class LoginPopup {

    @FXML
    private Button connectButton;
    @FXML
    private TextField usernameInput, hostInput, portInput;

    private String[] returns;

    public LoginPopup() {

    }

    @FXML
    private void initialize() {
        usernameInput.textProperty().addListener((observableValue, s, t1)
                -> connectButton.setDisable(invalidInput()));
        hostInput.textProperty().addListener((observableValue,s,t1)
                -> connectButton.setDisable(invalidInput()));
        portInput.textProperty().addListener((observableValue, s, t1)
                -> connectButton.setDisable(invalidInput()));

        connectButton.setOnAction(actionEvent->{
            String host = hostInput.getText();
            String port = portInput.getText();
            String username = usernameInput.getText();
            returns = new String[]{username, host, port};
            ((Stage)connectButton.getScene().getWindow()).close();
        });
    }

    public String[] display() throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = TestApp.getLoader("loginForm");
        Scene scene = new Scene(loader.load());
        LoginPopup controller = loader.getController();

        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        stage.setOnCloseRequest(windowEvent -> System.exit(0));

        stage.showAndWait();

        return controller.getData();
    }

    public String[] getData() {
        return returns;
    }

    public boolean invalidInput() {
        return usernameInput.getText().isBlank()
                || hostInput.getText().isBlank()
                || portInput.getText().isBlank()
                || !isInteger(portInput.getText());
    }

    public boolean isInteger(String text) {
        try {
            Integer.parseInt(text);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }



}
