package no.just_for_fun.chatapplication.client.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import no.just_for_fun.chatapplication.client.App;
import no.just_for_fun.chatapplication.server.message.Message;

import java.io.*;

public class TestController {
    @FXML
    private Button sendButton;
    @FXML
    private TextField textInput;
    @FXML
    private TextArea messageArea;

    @FXML
    private void initialize() {
        textInput.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode() == KeyCode.ENTER) {
                send();
            }
        });
    }

    @FXML
    private void send() {
        String text = textInput.getText();
        try {
            if (text.startsWith("/")) {
                App.sendRequest(new Message(text.substring(1),true));
            } else {
                App.sendMessage(new Message(text));
            }
        } catch (IOException e) {
            System.err.println("Failed to send message, " + e.getMessage());
        }
        textInput.clear();
    }

    public void printMessage(Message message) {
        messageArea.appendText(message.getMessage() + "\n");
    }
}
