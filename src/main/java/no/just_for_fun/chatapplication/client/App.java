package no.just_for_fun.chatapplication.client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import no.just_for_fun.chatapplication.client.controllers.LoginPopup;
import no.just_for_fun.chatapplication.client.controllers.TestController;
import no.just_for_fun.chatapplication.server.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class App extends Application {

    // TODO make login screen, connection screen, etc.

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private static Scene scene;

    private String userName = "user";
    private String host = "localhost";
    private int port = 8080;

    private static ChatClient client;

    private TestController controller;

    @Override
    public void start(Stage stage) throws Exception {

        // Setup scene
        setupScene(stage);

        // Show login form
        showLoginForm();

        // Connect to server
        connectToServer();

    }

    public static void sendMessage(Message message) throws IOException {
        client.sendMessage(message);
    }

    public static void sendRequest(Message message) throws IOException {
        client.sendRequest(message);
    }

    public void printMessage(Message message) {
        controller.printMessage(message);
    }

    public String getUserName() {
        return userName;
    }


    // methods for initialization

    public void showLoginForm() throws IOException {
        LoginPopup loginPopup = new LoginPopup();
        String[] data = loginPopup.display();

        userName = data[0];
        host = data[1];
        port = Integer.parseInt(data[2]);
    }

    public void setupScene(Stage stage) throws IOException, InterruptedException {
        LOGGER.debug("Setting up scene...");
        stage.setOnCloseRequest(windowEvent -> {
            try {
                client.disconnectUser();
            } catch (IOException e) {
                LOGGER.error("Failed to disconnect user",e);
            }
            System.exit(0);
        });

        FXMLLoader loader = getLoader("test");
        controller = loader.getController();
        scene = new Scene(loader.load());

        stage.setScene(scene);
        stage.setTitle("Username: [" + userName + "]");
        stage.show();

        LOGGER.debug("Scene set up!");

        LOGGER.info("Waiting for controller...");
        while (controller == null) {
            controller = loader.getController();
            Thread.sleep(100);
        }
    }

    public void connectToServer() {
        LOGGER.info("Connecting to server...");
        client = new ChatClient(host,port,this);
        client.start();
    }


    // JavaFX methods

    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    public static FXMLLoader getLoader(String fxml) {
        return new FXMLLoader(App.class.getResource("/" + fxml + ".fxml"));
    }

    public static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/" + fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}