module no.just_for_fun.chatapplication {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.slf4j;

    exports no.just_for_fun.chatapplication.client;
    exports no.just_for_fun.chatapplication.client.controllers;
    exports no.just_for_fun.chatapplication.server.message;

    opens no.just_for_fun.chatapplication.client.controllers to javafx.fxml;
    opens no.just_for_fun.chatapplication.client;
    exports no.just_for_fun.chatapplication.client.excpetions;
    opens no.just_for_fun.chatapplication.client.excpetions;

}